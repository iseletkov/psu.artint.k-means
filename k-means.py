#pip install pyqt5
#pip install pyqtchart


import sys
import inspect, os
import math
import random
import csv

from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QLabel, QPushButton, QLineEdit, QFileDialog
from PyQt5.QtGui import QIcon
from PyQt5.QtChart import QChart, QChartView, QScatterSeries

#********************************************************************************************************
#* Класс описывает одну машину.                                                                         *
#********************************************************************************************************
class CCar:
	def __init__(self, data):

		#Название модели в 0-м столбце.
		self.name = data[0]
		
		#Все остальные столбцы - параметры
		self.params = []
		
		for i in range(1, len(data)):
			#Меняем разделитель-запятую на точку, конвертируем в число.
			try:
				self.params.append(float(data[i].replace(',','.')))
			except Exception:
				self.params.append(0)
		return

#********************************************************************************************************
#* Класс описывает один кластер.                                                                        *
#********************************************************************************************************
class CCluster:
	def __init__(self):		
		self.cars = []
		self.param1 = None
		self.param2 = None
		
#********************************************************************************************************
#* Класс описывает форму.                                                                               *
#********************************************************************************************************
class CApplication(QWidget):
	#****************************************************************************************************
	#* Конструктор.                                                                        				*
	#****************************************************************************************************
	def __init__(self):
		super().__init__()
		self.initUI()
		
	#****************************************************************************************************
	#* Насройка компонент формы.                                                                        *
	#****************************************************************************************************
	def initUI(self):
		#Необходимость отладочных сообщений.
		self.verbose = False
		#Минимальное изменение в раположении центров кластеров, меньше которого считаем, что изменения больше не происходят.
		self.EPS = 10
		
		rowHeight = 20
		spacing = 10
		height = spacing
		columnWidth = 150
	
		self.setGeometry(300, 300, 5*spacing+2*columnWidth+500+rowHeight, 2*spacing+500)
		self.setWindowTitle('Кластеризация')
		self.setWindowIcon(QIcon('web.png'))
		
		labelFile = QLabel(self)
		labelFile.setText('Файл')
		labelFile.setParent(self)
		labelFile.move(spacing,spacing)
		labelFile.resize(columnWidth, rowHeight)
		
		self.labelFileName = QLineEdit(self)
		self.labelFileName.setParent(self)
		self.labelFileName.setReadOnly(True)
		self.labelFileName.move(2*spacing+columnWidth,spacing)
		self.labelFileName.resize(columnWidth, rowHeight)
		
		buttonFile = QPushButton(self)
		buttonFile.setParent(self)
		buttonFile.move(3*spacing+2*columnWidth,spacing)
		buttonFile.resize(rowHeight, rowHeight)
		buttonFile.clicked.connect(self.onButtonFileClick)
		
		height = height+spacing+rowHeight
		
		labelParam1 = QLabel(self)
		labelParam1.setText('Параметр 1 (1-7)')
		labelParam1.setParent(self)
		labelParam1.move(spacing,height)
		labelParam1.resize(columnWidth, rowHeight)
		
		self.leParam1 = QLineEdit(self)
		self.leParam1.setParent(self)
		self.leParam1.move(2*spacing+columnWidth,height)
		self.leParam1.resize(columnWidth+spacing+rowHeight, rowHeight)
		
		height = height+spacing+rowHeight
		
		labelParam2 = QLabel(self)
		labelParam2.setText('Параметр 2 (1-7)')
		labelParam2.setParent(self)
		labelParam2.move(spacing,height)
		labelParam2.resize(columnWidth, rowHeight)
		
		self.leParam2 = QLineEdit(self)
		self.leParam2.setParent(self)
		self.leParam2.move(2*spacing+columnWidth,height)
		self.leParam2.resize(columnWidth+spacing+rowHeight, rowHeight)
		
		height = height+spacing+rowHeight
		
		labelCount = QLabel(self)
		labelCount.setText('Количество кластеров (2-8)')
		labelCount.setParent(self)
		labelCount.move(spacing,height)
		labelCount.resize(columnWidth, rowHeight)
		
		self.leCount = QLineEdit(self)
		self.leCount.setParent(self)
		self.leCount.move(2*spacing+columnWidth,height)
		self.leCount.resize(columnWidth+spacing+rowHeight, rowHeight)
		
		height = height+spacing+rowHeight
		
		buttonCalculate = QPushButton(self)
		buttonCalculate.setParent(self)
		buttonCalculate.setText('Разбить на кластеры')
		buttonCalculate.move(spacing,height)
		buttonCalculate.resize(2*columnWidth+rowHeight+2*spacing, rowHeight)
		buttonCalculate.clicked.connect(self.onButtonCalculateClick)
		
		self.chart = QChart();
		self.chartView = QChartView(self.chart);
		self.chartView.setParent(self);
		self.chartView.move(2*columnWidth+rowHeight+3*spacing,spacing)
		self.chartView.resize(500, 500)
		self.show()
	#****************************************************************************************************
	#* Обработка нажатия на кнопку выбора файла.                                                        *
	#****************************************************************************************************	
	def onButtonFileClick(self):
		defaultPath = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
		fname = QFileDialog.getOpenFileName(self, 'Выбор файла', defaultPath)[0]
		
		self.labelFileName.setText(fname)
		self.cars = self.readCars()
		self.clusters = []
		
	#****************************************************************************************************
	#* Обработка нажатия на кнопку запуска расчёта.                                                     *
	#****************************************************************************************************	
	def onButtonCalculateClick(self):
		try:
			count = int(self.leCount.text())
			self.param1 = int(self.leParam1.text())-1;
			self.param2 = int(self.leParam2.text())-1;
		except Exception:
			print('Параметры алгоритма указаны некорректно!')
			return;
				
		for car in self.cars:
			print('x: '+str(car.params[self.param1])+' y: '+str(car.params[self.param2]))	
			
		#Начальные центры кластеров.			
		self.clusters = self.initializeClusters(count)
		if (self.verbose):
			self.outClusters();
		
		count = 0;
		#Итерации по уточнению центров кластеров.
		while True:
			self.distributeElements();
			change = self.recalculateCenters();
			if (self.verbose):
				self.outClusters();
			print('change: '+str(change));
			#Заканчиваем процесс подбора, когда изменения состава элементов прекращаются.
			if (change<self.EPS):
				break;
			if (count==8):
				print('Max iteration count reached. Exit program.');
				break;
			count = count+1;
		#Выводим результаты распределения на экран
		self.outGraphic();
		return;
		
	#****************************************************************************************************
	#* Считывает параметры автомобилей из csv. 															*
	#****************************************************************************************************
	def readCars(self):
		cars = []
		with open(self.labelFileName.text(), 'r') as csvfile:
			reader = csv.reader(csvfile, delimiter=';', quotechar='|')
		
			for row in reader:
				cars.append(CCar(row))
			
		return cars	
		
	#****************************************************************************************************
	#* Создаёт кластеры со случайными начальными значениями центров. 									*
	#****************************************************************************************************	
	def initializeClusters(self, count):
		max1 = self.findMaxParam(self.param1);
		max2 = self.findMaxParam(self.param2);
		
		clusters = [];
		for i in range(0, count):
			cluster = CCluster()
			cluster.param1 = random.random()*max1
			cluster.param2 = random.random()*max2
			clusters.append(cluster)
		return clusters
	#****************************************************************************************************
	#* Находит максимально возможное значение пераметра с указанным номером. 							*
	#****************************************************************************************************
	def findMaxParam(self, paramNum):
		max = 0;
		for car in self.cars:
			if car.params[paramNum]>max:
				max = car.params[paramNum];
		return max;
		
	#****************************************************************************************************
	#* Выводит данные кластеров в консоль. 																*
	#****************************************************************************************************
	def outClusters(self):
		for cluster in self.clusters:
			print('x: '+str(cluster.param1)+' y: '+str(cluster.param2)+' count: '+str(len(cluster.cars)));
		return;	
	#****************************************************************************************************
	#* Выводит данные на график. 																		*
	#****************************************************************************************************
	def outGraphic(self):
		#Удаляем все существующие ряды данных с графика.
		self.chart.removeAllSeries()
		
		seriesc = QScatterSeries();
		
		for cluster in self.clusters:
			seriesc.append(cluster.param1, cluster.param2);
		
			series = QScatterSeries();
			for car in cluster.cars:
				series.append(car.params[self.param1], car.params[self.param2])
			self.chart.addSeries(series)
			
		self.chart.addSeries(seriesc);
		
		self.chart.createDefaultAxes()
		return;
	#****************************************************************************************************
	#* Распределяет элементы по кластерам. 																*
	#* Центры у кластеров уже должны быть заданы.														*
	#****************************************************************************************************
	def distributeElements(self):
		#Удаляем ссылки на элементы из предыдущей итерации.
		for cluster in self.clusters:
			cluster.cars.clear()
		if (self.verbose):
			print('---distributeElements---');
		for car in self.cars:
			min = -1
			minCluster = None
			if (self.verbose):
				print('x: '+str(car.params[self.param1])+', y: '+str(car.params[self.param2])+', dist: ', end='');
			for cluster in self.clusters:
				distance = self.distance(car, cluster)
				if (min==-1):
					min = distance;
					minCluster = cluster;
				if (min>distance):
					min = distance;
					minCluster = cluster;
				if (self.verbose):
					print(str(distance)+', ', end='');
			if (self.verbose):
				print('min: '+str(min));	
			minCluster.cars.append(car);
		return;
	#****************************************************************************************************
	#* Производит перерасчёт центров кластеров по составу элементов. 									*
	#****************************************************************************************************
	def recalculateCenters(self):
		change = 0;
		if (self.verbose):
			print('---recalculateCenters---');
		for cluster in self.clusters:
			param1 = 0
			param2 = 0
			
			for car in cluster.cars:
				param1 = param1+car.params[self.param1];
				param2 = param2+car.params[self.param2];
				
			if (len(cluster.cars)>0):
				param1 = param1/len(cluster.cars);
				param2 = param2/len(cluster.cars);
			
			change = change + abs(param1-cluster.param1)+abs(param2-cluster.param2);
			if (self.verbose):
				print('param1: '+str(cluster.param1)+'->'+str(param1)+', param2: '+str(cluster.param2)+'->'+str(param2));
			
			cluster.param1 = param1;
			cluster.param2 = param2;
		return change;	
	#****************************************************************************************************
	#* Производит перерасчёт центров кластеров по составу элементов. 									*
	#****************************************************************************************************
	def distance(self, car, cluster):
		d1 = car.params[self.param1] - cluster.param1;
		d2 = car.params[self.param2] - cluster.param2;
		return math.sqrt(d1*d1+d2*d2);
		
#********************************************************************************************************
#* Основная функция программы.                                                                          *
#********************************************************************************************************
if __name__ == '__main__':
	app = QApplication(sys.argv)
	ex = CApplication()
	sys.exit(app.exec_())
